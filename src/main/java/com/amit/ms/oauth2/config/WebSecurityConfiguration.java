package com.amit.ms.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(1)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("joey")
			.password(passwordEncoder.encode("joey"))
			.roles("ADMIN");
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*
		 * http.authorizeRequests() .antMatchers("/login").permitAll()
		 * .antMatchers("/oauth/authorize") .authenticated() .and().formLogin()
		 * .and().requestMatchers() .antMatchers("/login", "/oauth/authorize");
		 */
		http.requestMatchers()
        	.antMatchers("/login", "/oauth/authorize")
        	.and()
        	.authorizeRequests()
        	.anyRequest().authenticated()
        	.and()
        	.formLogin().permitAll();
	}
}
