package com.amit.ms.oauth2.rest;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserDetailsResource {

	@GetMapping("/user/me")
	public Map<String, Object> user(Principal principal) {
		Map<String, Object> map = new HashMap<String, Object>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		map.put("username", principal.getName());
		map.put("authorities", authentication.getAuthorities());

		return map;
	}

	@GetMapping("/check")
	public String check() {
		return "Checked successfully!!";
	}
}
